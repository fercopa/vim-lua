noremap <silent> <leader>0 :lua require("harpoon.ui").toggle_quick_menu()<CR>
noremap <silent> <leader>k :lua require("harpoon.mark").add_file()<CR>
noremap <silent> <leader>1 :lua require("harpoon.ui").nav_file(1)<CR>
noremap <silent> <leader>2 :lua require("harpoon.ui").nav_file(2)<CR>
noremap <silent> <leader>3 :lua require("harpoon.ui").nav_file(3)<CR>
noremap <silent> <leader>4 :lua require("harpoon.ui").nav_file(4)<CR>
