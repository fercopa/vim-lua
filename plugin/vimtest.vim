nmap <silent> <leader>t :TestNearest<CR>
nmap <silent> <leader>T :TestFile<CR>
nmap <silent> <leader>a :TestSuite<CR>
" nmap <silent> <leader>l :TestLast<CR>
" nmap <silent> <leader>g :TestVisit<CR>

function! ToggleTerminalStrategy(command)
"echo It works! Command for running tests: . a:cmd
    exe 'TermExec cmd="' . a:command . '"'
endfunction

let g:test#custom_strategies = {'echo': function('ToggleTerminalStrategy')}
let g:test#strategy = 'echo'
let test#python#runner = 'pytest'
" let test#strategy = 'neovim'
"let test#strategy = 'harpoon'
"let g:test#basic#start_normal = 1
let g:test#preserve_screen = 1
let g:test#neovim#start_normal = 1
let test#python#pytest#options = "-xsv --color=yes"
" let test#neovim#term_position = vert
"let test#python#pytest#executable = 'docker-compose exec django pytest -xsv --color=yes'
