let g:ale_linters_explicit = 1
let g:ale_set_signs = 1
let g:ale_linters = {'python': ['flake8', 'bandit', 'pylint']}
let g:ale_fixers = {'*': [], 'python': ['black', 'isort']}
" let g:ale_linters_ignore = {'python': ['pylint']}
let g:ale_linters_ignore = ['pylint']
let g:ale_fix_on_save = 0
let g:ale_sign_error = '•'
let g:ale_sign_warning = '•'
highlight ALEErrorSign ctermfg=9 guifg=#fd4949
highlight ALEWarningSign ctermfg=11 guifg=#ffd242
