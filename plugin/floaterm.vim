" Configuration example
let g:floaterm_keymap_prev   = '<F8>'
let g:floaterm_keymap_next   = '<F9>'
let g:floaterm_keymap_toggle = '<F12>'
hi Floaterm guibg=black
let g:floaterm_wintype = 'floating'
