local utils = require('utils')
local cmd = vim.cmd
utils.opt('o', 'termguicolors', true)

vim.cmd "hi clear"
vim.cmd "syntax reset"
vim.cmd "let g:airline_theme='base16'"
vim.cmd "let g:jellybeans_overrides = {'background': { 'guibg': '#000507' }, 'String': {'guifg': '58ff74' },'Function': {'guifg': 'f98921' },}"
vim.cmd "  let g:airline_powerline_fonts = 1"
-- cmd 'colorscheme jellybeans'
-- cmd 'colorscheme onehalflight'
cmd 'colorscheme onehalfdark'
cmd([[
let s:mybg = 'dark'
function! BgToggleSol() 
    if (s:mybg ==? 'light')
       colors onehalfdark
       let s:mybg = 'dark'
    else
       colors onehalflight
       let s:mybg = 'light'
    endif
endfunction
]])
cmd "nnoremap <silent> <F2> :call BgToggleSol()<cr>"
