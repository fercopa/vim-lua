local utils = require('utils')

local cmd = vim.cmd
local indent = 4

cmd 'syntax enable'
cmd 'filetype plugin indent on'
utils.opt('b', 'expandtab', true)
utils.opt('b', 'shiftwidth', indent)
utils.opt('b', 'smartindent', true)
utils.opt('b', 'tabstop', indent)
utils.opt('o', 'hidden', true)
utils.opt('o', 'ignorecase', true)
utils.opt('o', 'scrolloff', 4 )
utils.opt('o', 'shiftround', true)
utils.opt('o', 'smartcase', true)
utils.opt('o', 'splitbelow', true)
utils.opt('o', 'splitright', true)
utils.opt('o', 'wildmode', 'list:longest')
utils.opt('w', 'number', true)
utils.opt('w', 'wrap', false)
utils.opt('w', 'cursorline', true)
utils.opt('o', 'clipboard','unnamed,unnamedplus')
utils.opt('o', 'cc', '120')
utils.opt('o', 'tabstop', indent)
utils.opt('o', 'softtabstop', indent)
utils.opt('o', 'wildmenu', true)

-- Highlight on yank
vim.cmd 'au TextYankPost * lua vim.highlight.on_yank {on_visual = false}'

-- clear empty spaces at the end of lines on save of python files
-- vim.cmd "au BufWritePre *.py :%s/\s\+$//e"

vim.cmd 'au FileType python map <silent> <leader>b Oimport ipdb; ipdb.set_trace()<esc>'
vim.cmd 'au FileType htmldjango setlocal shiftwidth=2 tabstop=2 softtabstop=2'
vim.cmd 'au FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=2'
vim.cmd "set wildmode=longest:full,list"
-- vim.cmd 'highlight Search guibg=#044165 guifg=white'
-- vim.cmd 'highlight CursorLine guibg=#202021 ctermbg=234'
-- vim.cmd 'highlight ColorColumn guibg=#5e5e5e'
--
vim.cmd "set t_8f=^[[38;2;%lu;%lu;%lum"
vim.cmd "set t_8b=^[[48;2;%lu;%lu;%lum"
vim.cmd "set laststatus=2"
-- vim.cmd "let g:airline_stl_path_style = 'short'"
vim.cmd "let g:airline#extensions#branch#displayed_head_limit = 25"
-- vim.cmd "let g:airline_statusline_ontop=1"

-- remove the filetype part
vim.cmd "let g:airline_section_x=''"
-- remove separators for empty sections
vim.cmd "let g:airline_skip_empty_sections = 1"
