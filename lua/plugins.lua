return require('packer').startup(function()
  -- Packer can manage itself as an optional plugin
  use {'wbthomason/packer.nvim', opt = true}
  -- Color scheme
  use { 'sainnhe/gruvbox-material' }
  -- Fuzzy finder
  use {
      'nvim-telescope/telescope.nvim',
      requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}
  }
  -- LSP and completion
  use {'neovim/nvim-lspconfig'}
  use {'nvim-lua/completion-nvim'}
  -- Lua development
  use {'tjdevries/nlua.nvim'}
  -- Vim dispatch
  use {'tpope/vim-dispatch'}
  -- Fugitive for Git
  use {'tpope/vim-fugitive'}
  use {'preservim/nerdtree'}
  -- Airline
  use {'vim-airline/vim-airline'}
  use {'vim-airline/vim-airline-themes'}
  -- Indent text object
  -- use {'michaeljsmith/vim-indent-object'}
  -- Better language packs
  use {'sheerun/vim-polyglot'}
  -- Highlight matching html tags
  use {'valloric/MatchTagAlways'}
  use {'andymass/vim-matchup'}
  use {'jiangmiao/auto-pairs'}
  -- Generate html in a simple way
  use {'mattn/emmet-vim'}
  use {'airblade/vim-gitgutter'}
  use {'justinmk/vim-sneak'}
  -- Nice icons in the file explorer and file type status line.
  use {'ryanoasis/vim-devicons'}
  use {'kyazdani42/nvim-web-devicons'}
  use {'dense-analysis/ale'}
  -- Consoles as buffers (neovim has its own consoles as buffers)
  use {'rosenfeld/conque-term'}
  -- XML/HTML tags navigation (neovim has its own)
  use {'vim-scripts/matchit.zip'}
  use {'jmcantrell/vim-virtualenv'}
  use {'voldikss/vim-floaterm'}
  use {"akinsho/toggleterm.nvim"}
  use {'nanotech/jellybeans.vim'}
  use {'kjssad/quantum.vim'}
  use {'haystackandroid/vim-crunchbang'}
  use {'cange/vim-theme-bronkow'}
  use {'overcache/NeoSolarized'}
  use {'preservim/tagbar'}
  use {'NLKNguyen/papercolor-theme'}
  use {'ayu-theme/ayu-vim'}
  use {'sonph/onehalf', rtp='vim/'}
  use {'nvim-treesitter/nvim-treesitter'}
  use {'crispgm/nvim-tabline'}
  use {'vim-test/vim-test'}
  use { "rcarriga/vim-ultest", requires = {"vim-test/vim-test"}, run = ":UpdateRemotePlugins" }
  use { 'luisdavim/pretty-folds' }
  use {
      "NTBBloodbath/rest.nvim",
      requires = { "nvim-lua/plenary.nvim" },
      config = function()
          require("rest-nvim").setup({
                  -- Open request results in a horizontal split
                  result_split_horizontal = false,
                  -- Skip SSL verification, useful for unknown certificates
                  skip_ssl_verification = false,
                  -- Highlight request on run
                  highlight = {
                      enabled = true,
                      timeout = 150,
                  },
                  result = {
                      -- toggle showing URL, HTTP info, headers at top the of result window
                      show_url = true,
                      show_http_info = true,
                      show_headers = true,
                  },
                  -- Jump to request line on run
                  jump_to_request = false,
                  env_file = '.env',
                  custom_dynamic_variables = {},
                  yank_dry_run = true,
              })
      end
  }
  use {
    'ThePrimeagen/harpoon',
    requires = {
        'nvim-lua/plenary.nvim',
        'nvim-lua/popup.nvim'
    }
  }
end)
